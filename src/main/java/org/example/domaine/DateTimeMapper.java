package org.example.domaine;

import java.time.Instant;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;

public class DateTimeMapper {

    public Instant offsetDtToInstant(OffsetDateTime offsetDt) {
        return offsetDt.toInstant();
    }

    public OffsetDateTime instantToOffsetDt(Instant instant) {
        return OffsetDateTime.ofInstant(instant, ZoneOffset.UTC);
    }
}
