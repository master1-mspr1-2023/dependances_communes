package org.example.infrastructure;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.time.OffsetDateTime;

@JsonIgnoreProperties(ignoreUnknown = true)
public record ErpOrderDTO(OffsetDateTime createdAt, String id, String customerId) {
}
