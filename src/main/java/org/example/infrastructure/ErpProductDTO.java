package org.example.infrastructure;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.time.OffsetDateTime;

@JsonIgnoreProperties(ignoreUnknown = true)
public record ErpProductDTO(OffsetDateTime createdAt, String name, ErpProductDetailsDTO details, int stock, String id) {
}
